

function Modal(props) {

    return (
        <div class="modalWrapper">
            <div className={props.className}>
                <header class="modal_header" style={props.style}>
                    <span class="header_title">{props.header}</span>
                    <span class="iconClose" onClick={props.closeButton}>✕</span>
                </header>
                <div class="modal_content">
                    <p>{props.text}</p>
                    <div>
                        {props.confirmButton}
                        {props.cancelButton}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Modal;