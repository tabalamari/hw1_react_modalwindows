import Button from './Components/Button';
import Modal from './Components/Modal';
import './App.css';
import './Styles/firstButton.scss';
import './Styles/secondButton.scss';
import './Styles/firstModal.scss';
import './Styles/secondModal.scss';

import { useState } from 'react';


function App() {
    const [firstModalOpened, setFirstModalOpened] = useState(false);
    const [secondModalOpened, setSecondModalOpened] = useState(false);

    return (
        <>
            <Button className="firstButton" onClick={() => setFirstModalOpened(true)} title="Open first modal" />
            <Button className="secondButton" onClick={() => setSecondModalOpened(true)} title="Open second modal" />
            {firstModalOpened ? <Modal closeButton={() => setFirstModalOpened(false)} className="firstModal" style={{backgroundColor: "#D44637"}} header="Do you want to delete this file?" text={"Once you delete this file, it won’t be possible to undo this action.\n Are you sure you want to delete it?"} confirmButton={<Button className="confCancelButton" title="Ok" style={{backgroundColor: "#B3382C" }}/>} cancelButton={<Button className="confCancelButton" title="Cancel" style={{backgroundColor: "#B3382C" }}/>}/> : null}
            {secondModalOpened ? <Modal closeButton={() => setSecondModalOpened(false)} className="secondModal" style={{backgroundColor: "#2b3ee5"}} header="Do you really want to fly to Mars?" text={"If you press this button, you will fly to Mars and may\nnever be able to go back."} confirmButton={<Button className="confCancelButton" title="I agree" style={{backgroundColor:"#0a097a"}}/>} cancelButton={<Button className="confCancelButton" title="I'm afraid" style={{backgroundColor:"#0a097a"}} />} /> : null}
        </>
    );
}


export default App;
